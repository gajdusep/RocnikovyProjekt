import os
import copy
import numpy as np
import operator
import DTW
import midi


"""
def play_music(music_files):
    clock = pygame.time.Clock()
    try:
        for i in range(0,len(music_files)):
            pygame.mixer.Channel(i).play(pygame.mixer.Sound(music_files[i]))
        #pygame.mixer.Channel(0).play(pygame.mixer.Sound(music_file))
        #pygame.mixer.music.load(music_file)
        #print("Music file %s loaded!" % music_file)
    except pygame.error:
        #print("Files %s not found! (%s)" % (music_file, pygame.get_error()))
        return
    pygame.mixer.music.play()
    while pygame.mixer.music.get_busy():
        # check if playback has finished
        clock.tick(30)
"""


def changeTempoOfPattern(pattern, multiplIndex):
    if multiplIndex < 0:
        return pattern
    toret = copy.deepcopy(pattern)
    toret.resolution = toret.resolution * multiplIndex
    return toret


def transpose(pattern, halftones: int, up: bool):
    """
    :param pattern: midi pattern
    :param halftones: number of halftones to shift
    :param up: boolean, true = up, false = down
    :return:
    """
    if not up:
        halftones = -halftones
    toret = copy.deepcopy(pattern)
    for track in toret:
        for event in track:
            if isinstance(event, midi.NoteEvent):
                pitch = event.get_pitch()
                if pitch + halftones < 0:
                    print(pitch)
                event.set_pitch(pitch + halftones)
    return toret

class MIDIMatrixHandler:
    def __init__(self, pattern):
        self.pattern = copy.deepcopy(pattern)
        self.dict = {} # sorted: key = tick, value = note events


    def makeDictOfPattern(self):
        for track in self.pattern:
            sumoftick = 0
            for event in track:
                sumoftick += event.tick
                if isinstance(event, midi.NoteEvent):
                    if sumoftick not in self.dict:
                        self.dict[sumoftick] = [event]
                    else:
                        self.dict[sumoftick].append(event)
        self.dict = sorted(self.dict.items(), key=operator.itemgetter(0))

    def makeMIDImatrix(self, framesPerSecond):
        """
            resolution = Ticks per Beat
            bpm = Beats per Minute
            in: framesPerSecond
            How many ticks per one frame???
                (beatsPerSecond) * TicksPerBeat = ticksPerSecond
                TICKSPERFRAME = ticksPerSecond / framesPerSecond
            tickPerFrame = ((bpm / 60) * res) // framesPerSecond
        """

        res = self.pattern.resolution
        tempoDict = self.getDictOfTempos(self.pattern)

        """ why isn't possible to use: for key, value in tempoDict???? """
        min_bpm = min(tempoDict.values())

        tickPerFrame = ((min_bpm / 60) * res) // framesPerSecond
        numberOfTick = self.dict[len(self.dict)-1][0]

        maxNote = 128  # big interval enough to have meaningful notes in it
        minNote = 0
        sizeX = maxNote - minNote
        sizeY = numberOfTick // tickPerFrame

        """Matrix of three dimensions: frame, pitch, beginning of the notes (true/false)"""
        self.matrixOfFrames = np.full((int(sizeY), int(sizeX), 2), 0, dtype="uint8")
        vector = np.full((1, sizeX), 0, dtype="uint8")

        """MAKING MATRIX"""
        lastFilled = 0
        actTPF = ((tempoDict[0] / 60) * res) // framesPerSecond
        for i in range(0, len(self.dict)):
            listOfNotes = self.dict[i][1]
            actTick = self.dict[i][0]

            # TODO: edit computing of actTPF...
            actFrame = int(actTick // actTPF)

            """fill remaining spaces"""
            for j in range(lastFilled, actFrame):
                for k in range(0, len(vector[0])):
                    self.matrixOfFrames[j][k][0] = vector[0][k]
            for k in range(0, len(vector[0])):
                self.matrixOfFrames[actFrame-1][k][1] = vector[0][k]

            """update vector of playing notes"""
            for note in listOfNotes:
                if note.velocity == 0:
                    vector[0][note.pitch - minNote] = 0
                else:
                    vector[0][note.pitch - minNote] = 1

            lastFilled = actFrame



        size = self.matrixOfFrames.shape
        controlingMatrix = np.full((size[0],size[1]), 0, dtype="uint8")
        for j in range(0,size[0]):
            for i in range(0, size[1]):
                controlingMatrix[j][i] = self.matrixOfFrames[j][i][0] #+ self.matrixOfFrames[j][i][1]

        return self.matrixOfFrames


    def getDictOfTempos(self, pattern):
        """ Find out changes of tempo """
        tempoDict = {}  # info about tempo changes
        ticks = 0
        for event in pattern[0]:
            ticks += event.tick
            if isinstance(event, midi.SetTempoEvent):
                tempoDict.update({ticks:event.get_bpm()})
                # tempoDict[str(ticks)] = event.get_bpm()
        return tempoDict

    def makePatternFromMatrix(matrix):
        print("not implemented yet")

    def makeIntervals(self, lengthsInFrames):
        listOfNPArr = []
        for i in range(0,lengthsInFrames):
            for j in range(0, self.matrixOfFrames.shape[0] - lengthsInFrames):
                listOfNPArr.append(self.matrixOfFrames[j:j+lengthsInFrames,])

        return listOfNPArr

    """
    acttempo = tempoDict.get(0)
    for track in pattern:
        sumofticks = 0
        indexinvectorLast = 0
        for event in track:
            # add ticks to absolute number of ticks
            tick = event.tick
            sumofticks += tick

            # fill remaining spaces
            indexinvector = sumofticks // tickPerFrame
            for i in range(indexinvectorLast, indexinvector):
                for j in range(sizeX):
                    matrixOfFrames[i][j] = vector[0][j]

            if isinstance(event, midi.NoteEvent):
                pitch = event.pitch
                index = pitch - minNote

            # find right tempo
            for key in tempoDict:
                if tempoDict[key] > sumofticks:
                    acttempo = tempoDict[key]

            indexinvectorLast = indexinvector
    """

def main():
    print("running spoiler.py")
    # pygame.init()
    # pygame.mixer.music.set_volume(0.8)

    pattern = midi.read_midifile("MSMD-MIDI-samples_2018-03-09/muss1.midi")
    transposed = transpose(pattern, 12, 0)

    m = MIDIMatrixHandler(pattern)
    m.makeDictOfPattern()
    sample1 = m.makeMIDImatrix(20)


    pattern2 = midi.read_midifile("MSMD-MIDI-samples_2018-03-09/bach1.midi")
    m = MIDIMatrixHandler(pattern2)
    # m = MIDIMatrixHandler(transposed)
    m.makeDictOfPattern()
    sample2 = m.makeMIDImatrix(20)

    DTW.midi_matrix_example(sample1, sample2)

    #list = m.makeIntervals(150)

    midi.write_midifile("MSMD-MIDI-samples_2018-03-09/transposed.midi", transposed)

    """
    listOfMidiFile = ("example.midi", "transposed.midi")
    try:
        play_music("pokus.mid")
    except KeyboardInterrupt:
        pygame.mixer.music.stop()

    """
    print()




if __name__ == "__main__":
    main()