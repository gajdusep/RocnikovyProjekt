from app.DTWWrapper import *
import os
from muscima.io import parse_cropobject_list
import cv2
from muscima.cropobject import *
from midiutil.MidiFile import MIDIFile
import numpy
import cv2


# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
# change this line -> False - fast loading of database but slow search
#                  -> True - very slow loading of db but faster search
READING_FILES_ON_SERVER_SETUP = False
# READING_FILES_ON_SERVER_SETUP = True


class DataDescriptor:
    """Data for wrapping the data of all kinds of files"""
    __imageSuffix = ".png"
    __thSuffix = ".jpg"
    __midiSuffix = ".mid"
    __pathToMidi = "CVC-MUSCIMA-detection/MIDI_pages/"
    __pathToFulls = "CVC-MUSCIMA-detection/images/fulls/"
    __pathToThumbnails = "CVC-MUSCIMA-detection/images/thumbnails/"

    def __init__(self, name):
        self.name = name
        self.imagePath = self.__pathToFulls + name + self.__imageSuffix
        self.midiPath = self.__pathToMidi + name + self.__midiSuffix
        self.thumbnailPath = self.__pathToThumbnails + name + "_tn" + self.__thSuffix
        path_to_full_images = 'app/static/CVC-MUSCIMA-detection/images/fulls/'
        img = cv2.imread(path_to_full_images + name + ".png")
        height, width, channels = img.shape
        self.height = height
        self.width = width

        # the possibility to load cropobjects when the server is loading - the search will be 100times faster
        if READING_FILES_ON_SERVER_SETUP:
            docs = [parse_cropobject_list('app/static/CVC-MUSCIMA-detection/cropobjects/complete/' + name + '.xml')]
            self.crop_objects = docs[0]
            print(name, ': ', len(self.crop_objects))


class FoundResultDescriptor:
    """Data class for wrapping the information about search results"""
    def __init__(self, name, cropped_path):
        self.name = name
        self.cropped_path = cropped_path
        self.imagePath = "CVC-MUSCIMA-detection/images/fulls/" + name + '.png'


class Database:
    """Data structure for keeping DataDescriptor data"""
    def __init__(self, path_to_fulls):
        self.__path_to_fulls = path_to_fulls
        self._database_dictionary = {}
        self._database_list = []
        self._DTWs_results = None

    @property
    def DTWs_results(self):
        """Returns pandas dataframe with results of all DTWs of all files"""
        if self._DTWs_results is None:
            results_name = 'app/result.csv'
            self._DTWs_results = pd.read_csv(results_name, index_col=0)
        return self._DTWs_results

    @property
    def database_dictionary(self):
        """database dictionary: key - the name of objects, value - DataDescriptor object"""
        if self._database_dictionary == {}:
            self._database_dictionary = self.make_dict(self.__path_to_fulls)
        return self._database_dictionary

    @property
    def database_list(self):
        """returns only values of database dictionary"""
        if self._database_list == []:
            self._database_list = self.database_dictionary.values()
        return self._database_list

    @staticmethod
    def make_dict(path_to_fulls):
        """Makes the database dictionary.
        Method goes through all pictures in given path and makes the DataDescriptro objects.
        The names of pictures in the folder are expected in the following format: name_of_picture.png"""

        print('started making dictionary')
        list = os.listdir(path_to_fulls)
        list_of_names = [item.split('.')[0] for item in list]

        database_dict = {}
        for item in list_of_names:
            data_descriptor = DataDescriptor(item)
            database_dict[data_descriptor.name] = data_descriptor

        print('dictionary made...')
        return database_dict


class Find:
    """Finding methods wrapper"""

    # static list of types of crop objects that need to be checked during the continuity check
    CONTINUITY_CHECK_TYPES_LIST = [
        'notehead-full',
        'notehead-empty'
        'whole_rest',
        'half_rest',
        'quarter_rest',
        '8th_rest',
        '16th_rest',
        '32th_rest',
        '64th_and_higher_rest',
        'multi-measure_rest'
    ]

    # names of crop_objects that is good to paint on the canvas
    PAINTING_RELEVANT_CROP_OBJECTS_NAMES = [
        'notehead-full',
        'notehead-empty'
        'whole_rest',
        'half_rest',
        'quarter_rest',
        '8th_rest',
        '16th_rest',
        '32th_rest',
        '64th_and_higher_rest',
        'multi-measure_rest'
        'flat',
        'sharp',
        'beam',
        'stem'
    ]

    @staticmethod
    def reconstruct_image(cropobjects, readlwidth, realheight):
        """
        Returns canvas with printed cropobjects
        :param cropobjects: list of crop objects with mask
        :param readlwidth: width of the picture
        :param realheight: height of the picture
        :return: 2D numpy array - canvas, all objects printed into one canvas
        """

        # Create the canvas onto which the masks will be pasted
        canvas = numpy.zeros((realheight, readlwidth), dtype='uint8')

        # put all objects to one canvas
        for c in cropobjects:
            pt = c.top
            pl = c.left

            # add actual object to canvas
            if pt + c.height < realheight and pl + c.width < readlwidth:
                canvas[pt:pt + c.height, pl:pl + c.width] += c.mask

        return canvas

    @staticmethod
    def write_cropobjects_to_png(cropobjects, path, width, height):
        """
        Writes given cropobjects list to png file to given path.
        :param cropobjects: list of CropObject
        :param path: path to png file
        :param width: width of original image
        :param height: height of original image
        :return:
        """
        can = Find.reconstruct_image(cropobjects, width, height)
        size1 = len(can)
        size2 = len(can[0])
        img = numpy.zeros([size1, size2, 4])
        img[:, :, 0] = can * 2
        img[:, :, 1] = can * 2
        img[:, :, 2] = can * 255
        img[:, :, 3] = numpy.ones([size1, size2]) * 150
        cv2.imwrite(path, img)

    @staticmethod
    def crop_objects_to_MIDIFile_object(note_objects: List[CropObject], tempo=120):
        """
        Makes MIDIFile from list of note-CropObjects
        :param note_objects: list of crop objects -> full-head and empty head objects
        :param tempo: tempo of midi
        :return: MIDIFile object
        """

        mf = MIDIFile(1)
        track = 0
        time = 0
        mf.addTrackName(track, time, "Sample Track")
        mf.addTempo(track, time, tempo)

        channel = 0
        volume = 100

        for note in note_objects:
            if (note.clsname == 'notehead-full' or note.clsname == 'notehead-empty') \
                    and 'midi_pitch_code' in note.data and 'duration_beats' in note.data and 'onset_beats' in note.data:
                pitch = note.data['midi_pitch_code']
                duration = note.data['duration_beats']
                time = note.data['onset_beats']
                mf.addNote(track, channel, pitch, time, duration, volume)

        return mf

    @staticmethod
    def write_crop_objects_to_midi_file(file_path, crop_objects: List[CropObject]):
        """
        Gets full path to midi file and list of note cropobjects and saves a new midi
        :param file_path: full path to midi file
        :param crop_objects: list of note cropobjects
        """

        mf = Find.crop_objects_to_MIDIFile_object(crop_objects)

        # write file to disk
        with open(file_path, 'wb') as out_file:
            mf.writeFile(out_file)

    @staticmethod
    def are_objects_continuous_tolerant_check(list_of_cropobjects: List[CropObject], all_crop_objects: List[CropObject]):
        """
        Tolerant method for continuity check.
        :param list_of_cropobjects: the list of CropObjects whose continuity needs to be checked.
        :param all_crop_objects: the list of all CropObjects from the the image that's being cropped
        :return: boolean - is continuous?
        """
        prepared_list = []

        for co in list_of_cropobjects:
            if co.clsname in Find.CONTINUITY_CHECK_TYPES_LIST:
                prepared_list.append(co)

        start = datetime.datetime.now()
        # check if objects are on line
        staffs = []
        for co in prepared_list:
            outlinks = co.get_outlink_objects(all_crop_objects)
            for outlink in outlinks:
                if outlink.clsname == 'staff':
                    if outlink not in staffs:
                        staffs.append(outlink)

        # objects were marked on only one staff
        if len(staffs) == 1:
            return True

        # objects were marked on more than one staff, check, if all the objects of all staffs are present
        all_objects_of_all_staffs_set = set([])
        for staff in staffs:
            staff_objects = staff.get_inlink_objects(all_crop_objects)
            staff_check_objects = []
            for i in staff_objects:
                if i.clsname in Find.CONTINUITY_CHECK_TYPES_LIST:
                    staff_check_objects.append(i)
            inlinks = [i.objid for i in staff_check_objects]
            all_objects_of_all_staffs_set = all_objects_of_all_staffs_set.union(set(inlinks))

        set_of_cropped_objects = set([x.objid for x in prepared_list])

        if len(all_objects_of_all_staffs_set.union(set_of_cropped_objects)) == len(set_of_cropped_objects):
            good = True
        else:
            good = False

        end = datetime.datetime.now()
        print('TIME continuity check', (end-start).seconds, 'secs, ', (end-start).microseconds, 'micsec')
        return good

    @staticmethod
    def are_objects_continuous_strict_check(list_of_cropobjects: List[CropObject]):
        """
        The strict method for continuity check.
        :param list_of_cropobjects: list of CropObjects whose continuity needs to be checked
        :return: boolean - is continuous?
        """

        # Prepare the list
        prepared_list = []

        for co in list_of_cropobjects:
            if co.clsname in Find.CONTINUITY_CHECK_TYPES_LIST:
                prepared_list.append(co)

        prepared_list.sort(key=lambda co: co.left)

        # list_to_check = [(x.data, x.objid) for x in prepared_list]
        # for i in list_to_check:
        #     print(i[1], i[0])

        for i in range(0, len(prepared_list) - 1):
            try:
                first = prepared_list[i].data['precedence_outlinks']
            except:
                first = []
            second = prepared_list[i+1].objid
            print('first:', first, '    second:', second)
            if second not in first:
                return False

        return True

    @staticmethod
    def crop_image_to_crop_objects(path_to_xml_file, left, right, top, bottom):
        """
        Reads the xml file with CropObjects, tries to crop it according to the borders
        :param path_to_xml_file: Path to the xml file with CropObjects
        :param left: left border
        :param right: Right border
        :param top: Top border
        :param bottom: Bottom border
        :return: None if the selected part is not continuous, List of CropObjects if the selected part is continuous
        """

        # read the xml file and save to the list of crop_objects -> doc
        docs = [parse_cropobject_list(path_to_xml_file)]
        doc = docs[0]

        # pick all relevant crop_objects within the given borders
        relevant_objects = []
        for crop_object in doc:
            if crop_object.left > left and crop_object.right < right \
                    and crop_object.bottom < bottom and crop_object.top > top\
                    and crop_object.clsname in Find.PAINTING_RELEVANT_CROP_OBJECTS_NAMES:
                relevant_objects.append(crop_object)

        # check the continuity
        is_continuous = Find.are_objects_continuous_tolerant_check(relevant_objects, doc)
        # is_continuous = Find.are_objects_continuous_strict_check(relevant_objects)

        if is_continuous:
            return relevant_objects
        else:
            return None

    @staticmethod
    def get_only_notes(cropobject_list: List[CropObject]):
        """
        Takes the list of CropObjects, selects only notes with needed data (midi_pitch_code, onset_beats),
        sorts it according to onset_beats.
        :param cropobject_list: list of CropObjects.
        :return: List of numbers - midi_pitch_codes, List of CropObjects - only note-CropObjects. Both sorted
        according to onset_beats
        """

        # print('number of notes without onset_beats or midi_pitch_code: ', len([x for x in cropobject_list if (
        #         (x.clsname == 'notehead-full' or x.clsname == 'notehead-empty')
        #         and ('midi_pitch_code' not in x.data or 'onset_beats' not in x.data)
        # )]))

        help_list = [x for x in cropobject_list if (x.clsname == 'notehead-full' or x.clsname == 'notehead-empty')
                     and 'midi_pitch_code' in x.data and 'onset_beats' in x.data]
        notes_as_cropobjects = sorted(help_list, key=lambda x: x.data['onset_beats'])
        notes_as_midi_pitch_codes = [x.data['midi_pitch_code'] for x in notes_as_cropobjects]
        return notes_as_midi_pitch_codes, notes_as_cropobjects

    @staticmethod
    def read_and_prepare(path):
        """
        Reads the xml file.
        :param path: path to xml file
        :return: List of numbers - midi_pitch_code, List of CropObjects - notes. Both sorted
        """
        start = datetime.datetime.now()
        docs = [parse_cropobject_list(path)]
        end = datetime.datetime.now()
        print('  --v-- TIME SPENT IN reading the xml doc:', (end - start).microseconds)

        doc = docs[0]
        numbers, number_co = Find.get_only_notes(doc)
        return numbers, number_co

    @staticmethod
    def find_according_to_image(needle, database, DTWs_results, n=10, left=0, right=-1, top=0, bottom=-1):
        """

        :param needle: DataDescriptor objects - the searched image
        :param database: database dictionary
        :param DTWs_results: pandas dataframe with all results
        :param n: the wanted number of results
        :param left:
        :param right:
        :param top:
        :param bottom:
        :return: List of ResultsObjects, path to image with highlighted notes
        """

        # path to xml files with saved data
        path_to_xml = 'app/static/CVC-MUSCIMA-detection/cropobjects/complete/'
        path_to_full_images = 'app/static/CVC-MUSCIMA-detection/images/fulls/'

        # try to crop the image and if the cropping is correct, get the list of the cropobjects. If not, return None
        cropped_cropobjects = Find.crop_image_to_crop_objects(path_to_xml + needle.name + '.xml', int(left), int(right),
                                                              int(top), int(bottom))
        if cropped_cropobjects is None:
            return None, None

        # if the cropping was successful, make image with highlighted cropobjects
        # image_file_path = Find.find_according_to_cropobjects(needle.name, left, right, top, bottom)
        needle_datadescriptor = database[needle.name]
        image_file_path = 'CVC-MUSCIMA-colorful-images/' + needle.name + \
                          'l' + str(left) + 'r' + str(right) + 't' + str(top) + 'b' + str(bottom) + ".png"
        image_file_path_to_write = 'app/static/' + image_file_path
        Find.write_cropobjects_to_png(cropped_cropobjects, image_file_path_to_write, needle_datadescriptor.width, needle_datadescriptor.height)

        # The continuity check is done, so get the list of notes (midi-pitch-codes)
        # and the list of cropobject (for making png file with highlighted notes)
        find_numbers, find_numbers_co = Find.get_only_notes(cropped_cropobjects)

        # get names of n closest files
        list_of_n = DTWWrapper.get_names_of_n_closest_files_from_dataframe(DTWs_results, needle.name, n)
        list_of_n = list_of_n.index.values

        start = datetime.datetime.now()
        part_result = []
        # for n closest files, count dtws on cuts, write the result to files
        for file in list_of_n:
            print('----------------')
            # print(file)

            # getting notes from file (DataDescriptor object)
            starttt = datetime.datetime.now()

            if file in database:

                data_descriptor = database[file]
                if READING_FILES_ON_SERVER_SETUP:
                    numbers, numbers_co = Find.get_only_notes(data_descriptor.crop_objects)
                else:
                    numbers, numbers_co = Find.read_and_prepare(path_to_xml + file + '.xml')
                enddd = datetime.datetime.now()
                print('TIME SPENT IN preparing subsequence', (enddd - starttt).microseconds)

                starttt = datetime.datetime.now()
                res1, dist = DTWWrapper.find_subsequence_dtw(find_numbers, numbers)
                # print('---')
                # print('found: ', list(numbers_co[i].data['midi_pitch_code'] for i in res1))
                # print('query: ', find_numbers)
                enddd = datetime.datetime.now()
                print('TIME SPENT IN finding subsequence', (enddd - starttt).microseconds)

                # name of file with highlighted notes
                image_file_name = file + 'l' + str(left) + 'r' + str(right) + 't' + str(top) + 'b' + str(bottom) + ".png"
                patth = 'app/static/CVC-MUSCIMA-found-colourful/' + image_file_name
                patth_to_show = 'CVC-MUSCIMA-found-colourful/' + image_file_name
                height = data_descriptor.height
                width = data_descriptor.width

                print(list(numbers_co[i].data['midi_pitch_code'] for i in res1))
                print(find_numbers)

                Find.write_cropobjects_to_png(list(numbers_co[i] for i in res1), patth, width, height)
                dd = FoundResultDescriptor(file, patth_to_show)
                part_result.append((dd, dist))

        # sort according to the distances
        result = [x[0] for x in sorted(part_result, key=lambda tup: tup[1])]

        print('before:  ', [x[0].name for x in part_result])
        print('after:   ', [x.name for x in result])

        end = datetime.datetime.now()
        print('TIME SPENT IN PART DTWS', (end - start).seconds)

        return result, image_file_path
