from muscima.io import *
import numpy as np
import pandas as pd
import os
import matplotlib.pyplot as plt
from matplotlib import cm
from dtaidistance import dtw as dtw2
import array
from fastdtw import fastdtw
from dtaidistance import dtw_visualisation as dtwvis
from typing import List
import datetime
from dtw import dtw
import time


class DTWWrapper:
    @staticmethod
    def find_subsequence_dtw(crop_objects_1, crop_objects_2):
        """

        :param crop_objects_1: the query, array of crop_objects
        :param crop_objects_2: the array in which we are searching the needle
        :return:
        """

        # divide the long file to smaller ones...
        # suppose the first one is shorter...
        constant_over = 3
        diff_const = 5
        len1 = len(crop_objects_1) + constant_over
        len2 = len(crop_objects_2)
        tuple_list = []

        # for every cut count dtw and save it to the list
        for i in range(0, len2 - len1, diff_const):
            dist, path = fastdtw(array.array('d', crop_objects_2[i:i + len1]), array.array('d', crop_objects_1))
            num = np.array(path)
            num = num.transpose()
            path = []
            path.append(num[0].tolist())
            path.append(num[1].tolist())
            tuple_list.append((dist, [], path, (i, i + len1)))

        if len(tuple_list) == 0:
            dist, path = fastdtw(array.array('d', crop_objects_2), array.array('d', crop_objects_1))
            num = np.array(path)
            num = num.transpose()
            path = []
            path.append(num[0].tolist())
            path.append(num[1].tolist())
            tuple_list.append((dist, [], path, (0, len(crop_objects_1))))

        # tuple [0] - dist, [1] - acc, [2] - path, [3] - indeces in longer list
        tuple_list.sort(key=lambda x: x[0])

        print(len(tuple_list))
        best_dist = tuple_list[0][0]
        best_path = tuple_list[0][2]
        best_indeces_list = range(tuple_list[0][3][0], tuple_list[0][3][1] + 2)
        # best_notes = crop_objects_2[best_indeces[0]:best_indeces[1]]

        # choose only the adequate ones:
        last_x = best_path[0][0]
        last_y = best_path[0][1]
        really_best_indeces = []
        for i in range(0, len(best_path[0])):
            if best_path[0][i] != last_x and best_path[1][i] != last_y:
                really_best_indeces.append(best_indeces_list[best_path[0][i-1]])
            last_x = best_path[0][i]
            last_y = best_path[1][i]

        return really_best_indeces, best_dist

    @staticmethod
    def get_names_of_n_closest_files_from_dataframe(df, file_name, n=10):
        """
        helping method for getting the closest files from prepared dataframe
        :param df: dataframe with prepared DTW scores
        :param file_name: name of the file to which we want to find the closest files
        :param n: number of closest files
        :return: array of strings, names of n closest files
        """

        n = df.shape[0] if n == 0 or n > df.shape[0] else n
        column = df[file_name].copy()
        column = column.sort_values().drop(file_name)
        return column[:n]

    @staticmethod
    def get_names_of_n_closest_files(path_to_dtw_results, file_name, n=10):
        # print(os.getcwd())
        df = pd.read_csv(path_to_dtw_results, index_col=0)
        return DTWWrapper.get_names_of_n_closest_files_from_dataframe(df, file_name, n)

    @staticmethod
    def prepare_dtws_for_all_files(xml_files_path):
        """
        Counts DTW results for all pairs of files
        :param xml_files_path: the path to the directory with XML files
        :return:
        """

        dir = os.getcwd()

        os.chdir(xml_files_path)
        print(os.getcwd())
        print(len(os.listdir()))

        column_headers = os.listdir()

        # comment this:
        # how_many_files = 20
        # column_headers = column_headers[0:how_many_files]

        all_files_cropobjects_preparing = []

        # select only notes
        for name in column_headers:
            docs = [parse_cropobject_list(name)]
            doc = docs[0]
            print('name: ', name)
            act_xml_file = [x for x in doc if (x.clsname == 'notehead-full' or x.clsname == 'notehead-empty')
                            and 'midi_pitch_code' in x.data and 'onset_beats' in x.data]
            all_files_cropobjects_preparing.append(act_xml_file)

        # sort notes according to time in the music
        for file in all_files_cropobjects_preparing:
            file.sort(key=lambda x: x.data['onset_beats'])

        # make arrays of midi_pitch_codes
        all_files_cropobjects = []
        for f in all_files_cropobjects_preparing:
            l = [x.data['midi_pitch_code'] for x in f]
            all_files_cropobjects.append(l)

        print('--- reading done ---')

        width = len(all_files_cropobjects)
        results = np.zeros((width, width), dtype=float)

        # count DTWs
        for i in range(0, width - 1):
            for j in range(i, width):
                dist2 = dtw2.distance_fast(array.array('d', all_files_cropobjects[i]),
                                           array.array('d', all_files_cropobjects[j]))
                dist2 = round(dist2, 2)
                print(dist2)

                results[i][j] = dist2
                results[j][i] = dist2

        headers = [xml_name.split('.')[0] for xml_name in column_headers]
        df = pd.DataFrame(results, columns=headers, index=headers)
        os.chdir(dir)

        return df
