
/* ------The thumbnail part----------*/

// Kick off the jQuery with the document ready function on page load
$(function(){
	imagePreview();
});

// Configuration of the x and y offsets
this.imagePreview = function(){
		xOffset = -20;
		yOffset = 40;

    $("a.preview").hover(function(e){
        this.t = this.title;
        this.title = "";
	    var c = (this.t != "") ? "<br/>" + this.t : "";
	    console.log(this.t);
        console.log(this.href);
        console.log(c);

        $("body").append("<p id='preview'><img src='" + this.href +"' alt='Image preview' width='600'/>"+ c +"</p>");
        $("#preview")
            .css("z-index", findHighestZIndex('div') + 1)
            .css("top",(e.pageY - xOffset) + "px")
            .css("left",(e.pageX + yOffset) + "px")
            .fadeIn("fast");
    },

    function(){
        this.title = this.t;
        $("#preview").remove();

    });

    // sets position of preview, depends on mouse position
    $("a.preview").mousemove(function(e){
        $("#preview")
            .css("top",(e.pageY - xOffset) + "px")
            .css("left",(e.pageX + yOffset) + "px");
    });

    // finds the highest z-index, sets the highest z-index to thumbnail preview
    function findHighestZIndex(elem)
        {
          var elems = document.getElementsByTagName(elem);
          var highest = 0;
          for (var i = 0; i < elems.length; i++)
          {
            var zindex=document.defaultView.getComputedStyle(elems[i],null).getPropertyValue("z-index");
            if ((zindex > highest) && (zindex != 'auto'))
            { highest = zindex;}
          }
          return highest;
        }
};

// holds coordinates of cropping rectangle
var croppingParams = {
    top: 0,
    bottom: 0,
    left: 0,
    right: 0
};

// function to get parameters from cropper and save them to croppingParams variable
function setCroppingParams() {
    croppingParams.left = Math.round($cropper.getData().x);
    croppingParams.top = Math.round($cropper.getData().y);
    croppingParams.right = croppingParams.left + Math.round($cropper.getData().width);
    croppingParams.bottom = croppingParams.top + Math.round($cropper.getData().height);
}

// after clicking at send params button
function sendParamsButton(e, picture){
    setCroppingParams();
    console.log(window.location.origin);
    var h = window.location.origin;
    window.location = h + "/find/" + picture +
        '/?t=' + croppingParams.top + '&b=' + croppingParams.bottom +
        '&l=' + croppingParams.left + '&r=' + croppingParams.right;
}

var $cropper;
var $image;
$(function() {
	$image = $('#image');
	var height = $image.height() + 4;

	$('.crop_preview').css({
		width: '100%', //width,  sets the starting size to the same as orig image
		overflow: 'hidden',
		height:    400,//height, // TODO: edit max width and height -> according to div proportions
		maxWidth:  800,//$image.width(),
		maxHeight: 400//height
	});

	// options for cropper -> important viewMode: 3, initialize sizes...
  	$image.cropper({
	  	dragMode: 'move',
	  	viewMode: 3, // denies cropper to get out of bounds
	  	autoCropArea: 1,
	  	restore: false,
	  	guides: false,
	  	center: false,
	  	highlight: false,
	  	//toggleDragModeOnDblclick: false,
	  	preview: '.crop_preview',
	  	ready: function (e) {
	  		// following numbers are initializating values, changed after cropping
	  		$(this).cropper('setData', {
	  			height: 1000,
				rotate: 0,
				scaleX: 1,
				scaleY: 1,
				width:  1000,
				x:      100,
				y:      100
			});
	  	},
	  	crop: function (event) {
			//console.log("x: " + event.detail.x);
			//console.log("widht: " + event.detail.width);
        }
  	});
  	$cropper = $image.data('cropper');
});

function homeFunction() {
    window.location = window.location.origin;
}

function goBackToCropping(pathToImage) {
    window.location = window.location.origin + '/display/' + pathToImage;
}
