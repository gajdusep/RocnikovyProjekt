from app import app
from flask import Flask, render_template, redirect, url_for, request
import sys
import os
from app.find import *
from app.DTWWrapper import DTWWrapper


# ----------- PREPARE THE SERVER AND THE DATABASE -----------

print('database making')

# prepare the database
db = Database("app/static/CVC-MUSCIMA-detection/images/fulls/")
pictures_dict = db.database_dictionary
pictures_list = db.database_list
DTWs_results = db.DTWs_results

print('database made')

# read the file with results od DTW of all pairs of files
DTWs_of_all_pair_of_files = None
if not os.path.isfile('app/result.csv'):
    path = 'app/static/CVC-MUSCIMA-detection/cropobjects/complete'
    list = DTWWrapper.prepare_dtws_for_all_files(path)
    list.to_csv('app/result.csv')
    DTWs_of_all_pair_of_files = list
else:
    # TODO: DTWs_of_all_pair_of_files = read_results
    DTWs_of_all_pair_of_files = None


# ----------- ROUTES -----------

# Main page, shows the galery from which user can choose a picture
@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html',
                           title='Home',
                           pictures_list=pictures_list,
                           requested_picture=None,
                           warning_visible="none")


# chosen image
@app.route('/display/<image_path>')
def display(image_path):
    image_path = image_path.split('.')[0]
    requested_picture = pictures_dict[image_path]
    print(requested_picture)
    return render_template('select_and_crop_page.html',
                           title='Display',
                           pictures_dict=pictures_dict,
                           pictures_list=pictures_list,
                           requested_picture=requested_picture,
                           warning_visible="none")


# List of found pictures_dict
@app.route('/find/<image_path>/')
def find(image_path):
    requested_picture = pictures_dict[image_path]
    top = request.args.get('t')
    bottom = request.args.get('b')
    left = request.args.get('l')
    right = request.args.get('r')

    start_search_time = datetime.datetime.now()
    results, image_file_path = \
        Find.find_according_to_image(requested_picture, pictures_dict, DTWs_results, 10, int(left), int(right), int(top), int(bottom))
    end_search_time = datetime.datetime.now()
    print('THE FINAL TIME OF SEARCH: ', (end_search_time - start_search_time).seconds)

    # if the cropping was not correct
    if results is None:
        return render_template('select_and_crop_page.html',
                               title='Display',
                               pictures_dict=pictures_dict,
                               pictures_list=pictures_list,
                               requested_picture=requested_picture)
    else:
        return render_template('found_pictures.html',
                               image_file_path=image_file_path,
                               results=results,
                               title='Display',
                               pictures=pictures_dict,
                               requested_picture=requested_picture)
