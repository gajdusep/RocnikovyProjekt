from app.find import *
from app.DTWWrapper import *


class FindTestWrapper:
    @staticmethod
    def test_DTWPreparer():
        if not os.path.isfile('result.csv'):
            path = 'static/CVC-MUSCIMA-detection/cropobjects/complete'
            list = DTWWrapper.prepare_dtws_for_all_files(path)
            list.to_csv('result.csv')

    @staticmethod
    def test_reading():
        path_results_file = 'result.csv'
        name_example = 'CVC-MUSCIMA_W-01_N-01_D-ideal.xml'
        n = 50

        list_of_n = DTWWrapper.get_names_of_n_closest_files(path_results_file, name_example, n)
        # print(list_of_n)
        # print(list(list_of_n.index))

    @staticmethod
    def test_find_subsequence():
        os.chdir('static/CVC-MUSCIMA-detection/cropobjects/complete')

        # the picture we are taking it from
        path1 = 'CVC-MUSCIMA_W-03_N-07_D-ideal.xml'
        numbers1, numbers1_co = Find.read_and_prepare(path1)

        # the same page from another writer
        numbers2, numbers2_co = Find.read_and_prepare('CVC-MUSCIMA_W-01_N-07_D-ideal.xml')

        # somehow similar page
        numbers3, numbers3_co = Find.read_and_prepare('CVC-MUSCIMA_W-01_N-09_D-ideal.xml')

        # very different page
        numbers4, numbers4_co = Find.read_and_prepare('CVC-MUSCIMA_W-01_N-04_D-ideal.xml')

        top = 448
        bottom = 686
        left = 991
        right = 2179
        some_objects = Find.crop_image_to_crop_objects(path1, left, right, top, bottom)
        find_numbers, find_numbers_co = Find.get_only_notes(some_objects)

        print(numbers1[:10])
        print(numbers2[:10])
        print(numbers3[:10])
        print(numbers4[:10])
        print(find_numbers[:10])

        res1 = DTWWrapper.find_subsequence_dtw(find_numbers, numbers1)
        print('---')
        print('found: ', list(numbers1_co[i].data['midi_pitch_code'] for i in res1))
        print('query: ', find_numbers)

        res2 = DTWWrapper.find_subsequence_dtw(find_numbers, numbers2)
        print('---')
        print('found: ', list(numbers2_co[i].data['midi_pitch_code'] for i in res2))
        print('query: ', find_numbers)

        res3 = DTWWrapper.find_subsequence_dtw(find_numbers, numbers3)
        print('---')
        print('found: ', list(numbers3_co[i].data['midi_pitch_code'] for i in res3))
        print('query: ', find_numbers)

        res4 = DTWWrapper.find_subsequence_dtw(find_numbers, numbers4)
        print('---')
        print('found: ', list(numbers4_co[i].data['midi_pitch_code'] for i in res4))
        print('query: ', find_numbers)

